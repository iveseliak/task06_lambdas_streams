package task3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Manager {

    public Double getAvg(List<Integer> list) {

        return list.stream().mapToInt(i->i).average().getAsDouble();
    }

    public Integer getSum(List<Integer> list) {

        return list.stream().mapToInt(i->i).sum();
    }

    public Integer getMax(List<Integer> list) {

        return list.stream().mapToInt(i->i).max().getAsInt();
    }

    public Integer getMin(List<Integer> list) {

        return list.stream().mapToInt(i->i).min().getAsInt();
    }

    public int getCountNumbersBiggerThanAverage(List<Integer> list){
        return (int) list.stream().filter(v->v>getAvg(list)).count();

    }

}
