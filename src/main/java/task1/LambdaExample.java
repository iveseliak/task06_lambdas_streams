package task1;

public class LambdaExample {
    public static void main(String[] args) {
        MyLambda myLambda=(a, b)-> Math.max(a, b);
            System.out.println(myLambda.fun1(10,33));

        MyLambda myLambda1=(a, b)->(a+b)/2;
        System.out.println(myLambda1.fun1(33,64));

    }
}
