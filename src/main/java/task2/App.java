package task2;

public class App {



    public static void main(String[] args) {

        Command commandThird=new Command(){
            public void addCommand(String text){
                System.out.println(text);
            }

        };
        commandThird.addCommand("Hello");

        CommandInterface commandInterface=(s)-> System.out.println(s);
        commandInterface.addCommand("Hello");

        CommandFirst commandFirst=new CommandFirst();
        commandFirst.addCommand("Hello");
    }
}
